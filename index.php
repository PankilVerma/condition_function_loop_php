<?php
echo"<h1> conditions_php </h1>";


//if conditions
$x ="hello";
$y ="hello";
if($x==$y)
{
    echo"your string length is matching";
}
echo"<br><br>";


//else_condition

$x ="hello";
$y ="bye";
if($x==$y)
{
    echo"your string length is matching";
}
else
{
    echo"your string length is not matching";
}
echo"<br> <br>";
//if-elseif

$x ="hello";
$y ="bye";
if($x==$y)
{
    echo"your string length is matching";
}
elseif($x===$y)
{
    echo"your string length is not matching";
}
else{
    echo" please try again";
}
echo"<br> <br>";
echo"<h1> Switch_statement</h1>";

//switch_statements

$favcolor="red";
switch ($favcolor="red") {
  case "n":
    echo "Your favorite color is red!";
    break;
  case "blue":
    echo "Your favorite color is blue!";
    break;
  case "green":
    echo "Your favorite color is green!";
    break;
  default:
    echo "Your favorite color is neither red, blue, nor green!";
}
echo"<br><br>";
switch ($favcolo="green") {
    case "red":
      echo "Your favorite color is red!";
      break;
    case "blue":
      echo "Your favorite color is blue!";
      break;
    case "green":
      echo "Your favorite color is green!";
      break;
    default:
      echo "Your favorite color is neither red, blue, nor green!";
  }
  echo"<br><br>";
  echo"<h1>while_loop</h1>";


  //while_loop

  $x = 1;
 
while($x < 5) {
    echo "The number is: $x <br>";
   $x++;  
}       
echo"<br><br>";
  echo"<h1>Do while_loop</h1>";


  //Do_while_loop
  $x = 1;
 do{  
    echo "The number is: $x <br>";
    $x++;
   
 }
while($x < 1);  
echo"<br><br>";
echo"<h1>For_loop</h1>";

//for_loop
for ($x = 0; $x <= 10; $x++) {
    echo "The number is: $x <br>";
  }  

//for_each_loop used in array//
echo"<br><br>";
echo"<h1>Foreach_loop</h1>";
$colors = array("red", "green", "blue", "yellow"); 

foreach ($colors as $value) {
  echo "$value <br>";
}
//break//
echo"<br><br>";
echo"<h1>Break</h1>";
for ($x = 0; $x < 10; $x++) {
    if ($x == 4) {
      break;//jump loop
    }
    echo "The number is: $x <br>";
  }
  //continue//
echo"<br><br>";
echo"<h1>continue</h1>";
for ($x = 0; $x < 10; $x++) {
    if ($x == 4) {
      continue; //skipiing
    }
    echo "The number is: $x <br>";
  }

//function
echo"<br> <br>";
echo"<h1>Function</h1>";
 
function first_prog(){ //function declare
  $x =10;
  $y =20;
  
  $z =$x+$y;
  echo"the first number is: $x"."<br>";
  echo"the second number is: $y"."<br>";
  echo"the sum of numbers is : $z";

}
first_prog(); //function_call

echo"<br> <br>";
function addNumbers(int $a, int $b) {
  return $a + $b;
}
echo addNumbers(5, 5); //return

?>